package assign3;

public class Cuboid extends Rectangle{
	private double height;
	private double width;
	private double length;
	Cuboid(double width,double length,double height){
		super(width,length);
		this.height = height < 0.0d?0.0d:height;
		this.width  = width;
		this.length = length;
	}
	double getHeight() {
		return height;
	}
	
	double getArea() {
		return width * length;
	}
	
	double getVolume() {
		return width * length * height;
	}	
}
