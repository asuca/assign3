package assign3;

public class Rectangle {
	private double width;
	private double length;
	Rectangle(double width,double length){
		this.width  = width < 0.0d?0.0d:width;
		this.length = length < 0.0d?0.0d:length;
	}
	double getWidth(){
		return width;
	}
	double getLength() {
		return length;
	}
	double getArea() {
		return width * length;
	}
}
